const express = require('express');
const expressSession = require('express-session');
const mongoose = require('mongoose');
const Events = require('events');
const { google } = require('googleapis');
const { OAuth2Client } = require('google-auth-library');
const { CronJob } = require('cron')
const { GetSheet } = require('./sheets')
const low = require('lowdb');
const moment = require('moment');
const memstore = require('memory-cache');
const FileSync = require('lowdb/adapters/FileSync');
const app = express();
const PORT = process.env.PORT || 16000;

const emitter = new Events()
const lowb = new FileSync('db.json')
const db = low(lowb)

const userSchema = {
  name: String,
  refreshToken: String,
  accessToken: String
}

let ssid = ''
mongoose.connect('mongodb://admin:admin123@ds159493.mlab.com:59493/matomo', { useCreateIndex: false, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
  .then(() => console.log(`MONGODB CONNECTED SUCCESSFULLY!!`))
  .catch(err => console.log(`ERROR CONNECTING TO THE MONGODB: ${JSON.stringify(err, null, 2)}`))

// register user schema
mongoose.model('user', userSchema)

const redirectURI = 'https://matomo-hostpapa-tracker.herokuapp.com/consent' // process.env.NODE_ENV === 'prod' ? 'https://matomo-hostpapa-tracker.herokuapp.com/consent' : `http://localhost:${PORT}/consent` //  'https://matomo-hostpapa-tracker.herokuapp.com/consent'
const client = new OAuth2Client({
  clientId: '140468521270-mi7329u26nap3bs0ad21bgd3e5s6toa5.apps.googleusercontent.com',
  clientSecret: 'd_9_ieixY2ad5wdqH29k7wLf',
  redirectUri: redirectURI
});

// set lowdb default
db.defaults({
  refresh_token: '',
  access_token: ''
});

// scopes we want to access on google sheet
const scopes = ['https://www.googleapis.com/auth/drive',
  'https://www.googleapis.com/auth/drive.file',
  'https://www.googleapis.com/auth/spreadsheets',]

this.authorize = client.generateAuthUrl({
  access_type: 'offline',
  scope: scopes
})

function getAuthURL() {
  let oauthClient = client
  let scopes = ['https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/spreadsheets',]

  let authURL = oauthClient.generateAuthUrl({
    access_type: 'offline',
    scope: scopes
  })

  return authURL
}

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(expressSession({
  secret: 'matomoguy',
  resave: false,
  saveUninitialized: false
}))

app.get('/', async (req, res) => {
  return res.status(200).json({ message: "HELLO THERE!" });
});

app.get('/app/:name', async (req, res) => {
  try {
    const { name } = req.params

    if (!name) {
      return res.status(401).json({ message: "please add your name at the end of the url. all lowercase. For example: jacob" })
    }

    const localClient = new OAuth2Client({
      clientId: '140468521270-mi7329u26nap3bs0ad21bgd3e5s6toa5.apps.googleusercontent.com',
      clientSecret: 'd_9_ieixY2ad5wdqH29k7wLf',
      redirectUri: redirectURI
    });

    req.session.user = name

    let muser = await mongoose.model('user').findOne({ name: name })

    if (!muser) {
      await mongoose.model('user').create({ name: req.params.name })
      let url = localClient.generateAuthUrl({
        access_type: 'offline',
        scope: scopes,
        prompt: 'consent'
      })
      return res.redirect(url)
    }
    let { accessToken, refreshToken } = muser

    // the credentials have expired
    if (!accessToken) {
      const url = localClient.generateAuthUrl({
        access_type: 'offline',
        scope: scopes,
        prompt: 'consent'
      });

      return res.redirect(url);
    } else {
      client.setCredentials({
        refresh_token: refreshToken
      })
      emitter.emit('avail')
      return res.status(200).json({ message: 'all is well.. there is nothing to see here' });
    }
  } catch (e) {
    console.log(`ERROR WITH APP HERE::`, e)
    return res.status(500).json({ message: 'internal server error' })
  }
});

emitter.on('avail', function () {
  mongoose.model('user').findOne({ name: 'jacob' }).then(r => {
    const { refreshToken } = r
    client.setCredentials({
      refresh_token: refreshToken
    })

    GetSheet(client, null, null)
  })
    .catch(err => console.log(`ERROR`))
})

app.get('/consent', async (req, res) => {
  try {
    let code = req.query.code;
    const { tokens } = await client.getToken(code)
    let { user } = req.session
    console.log(`REFRESH TOKEN IS::`, tokens.refresh_token)
    await mongoose.model('user').findOneAndUpdate({ name: user }, { refreshToken: tokens.refresh_token, accessToken: tokens.access_token }, { upsert: true })

    client.setCredentials(tokens)
    return res.redirect(`/app/${req.session.user}`)
  } catch (e) {
    console.log(`ERROR GIVING CONSENT`)
    console.log(`=========================`)
    console.log(e)
    console.log(`=========================`)
  }
})


/**
 * ALL THINGS CRON JOB.. THIS FIRES THE DAILY JOB
 */
const job = new CronJob('00 30 06 * * *', function () {
  console.log(`TIME TO FIRE FOR DAY::`)
  mongoose.model('user').findOne({ name: 'jacob' }).then(r => {
    const { refreshToken } = r
    client.setCredentials({
      refresh_token: refreshToken
    })

    GetSheet(client, null, null)
      .then(r => console.log(`ACTION FIRED FOR THE DAY: ${moment().format('YYYY-MM-DD')}`))
      .catch(err => console.log(`ERROR FIRING ACTION FOR THE DAY.`))

  })
    .catch(err => console.log(`ERROR GETTING USER BKA BKAD`))
});

// fire job
job.start();


// RUN EVERY 7 AM ON MONDAY
const weeklyJob = new CronJob('00 00 7 * * 1', function () {
  mongoose.model('user').findOne({ name: 'jacob' }).then(r => {
    const { refreshToken } = r
    client.setCredentials({
      refresh_token: refreshToken
    })
  })

  GetSheet(client, 'last2', 'week')
})

// start weekly job
weeklyJob.start()

// RUN EVERY FIRST DAY OF THE MONTH
const monthlyJob = new CronJob('00 00 7 * *', function () {
  mongoose.model('user').findOne({ name: 'jacob' }).then(r => {
    const { refreshToken } = r
    client.setCredentials({
      refresh_token: refreshToken
    })
  })

  GetSheet(client, 'last2', 'month')
})

// start monthly job
monthlyJob.start()


app.get('*', async (req, res) => {
  const path = req.path;
  return res.status(200).json({ message: `HELLO, STRANGER. WELCOME TO: ${path}` });
});

app.listen(PORT, err => {
  if (err) {
    console.log(`ERROR LISTENING TO SERVER`);
    throw err;
  };

  console.log(`SERVER UP AND RUNNING ON PORT: ${PORT}`);
});
