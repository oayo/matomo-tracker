const { GetChannelDataSums, GetCustomReportDataSums } = require('./matomo');


const nf = [{ id: 2, tld: '.ca' }, { id: 3, tld: '.com' }, { id: 4, tld: '.com.au' }, { id: 5, tld: '.eu' }, { id: 6, tld: 'co.nz' }, { id: 7, tld: '.sg' }, { id: 8, tld: 'co.uk' }, { id: 9, tld: '.com.mx' }, { id: 10, tld: '.fr' }, { id: 11, tld: '.ie' }, { id: 12, tld: '.be' }, { id: 13, tld: '.de' }, { id: 14, tld: '.in' }, { id: 17, tld: '.hk' }, { id: 18, tld: '.es' }]
let labelSums = (label, obj, tag) => obj.filter(x => x.label === label).map(x => tag && tag === 'conv' ? (!x.conversions ? 0 : x.conversions) : x.visits).reduce((x, y) => x + y, 0);

async function ChannelDataSums(wp, resource, date, period) {
  try {
    let resp = []
    for (let site of nf) {
      let chanSums = await GetChannelDataSums(site.id, period ? period : 'day', date ? date : 'yesterday', wp)
      resp.push(...chanSums)
    }
    let labels = ['Direct Entry', 'Campaigns', 'Search Engines', 'Websites', 'Social Networks']
    let res = labels.map(x => labelSums(x, resp, resource))
    return res
  } catch (e) {
    console.log(`ERROR GETTING CHANNEL DATA SUMS HERE`)
    console.log(e)
    return e
  }
};

async function MediumDataSums(wp, resource, date, period) {
  try {
    let resp = []
    for (let site of nf) {
      let chanSums = await GetCustomReportDataSums(site.id, period ? period : 'day', date ? date : 'yesterday', wp)
      resp.push(...chanSums)
    }
    let labels = ['affiliate', 'email', 'paid-search', 'feedersites']
    let res = labels.map(x => labelSums(x, resp, resource))
    return res
  } catch (e) {
    console.log(`ERROR GETTING MEDIUM DATA SUMS HERE`)
    console.log(e)
    return e
  }
};


module.exports = { ChannelDataSums, MediumDataSums }