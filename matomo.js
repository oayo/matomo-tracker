const axios = require('axios');
const _ = require('lodash');


// (async () => {
//     const nf = [{ id: 2, tld: '.ca' }, { id: 3, tld: '.com' }, { id: 4, tld: '.com.au' }, { id: 5, tld: '.eu' }, { id: 6, tld: 'co.nz' }, { id: 7, tld: '.sg' }, { id: 8, tld: 'co.uk' }, { id: 9, tld: '.com.mx' }, { id: 10, tld: '.fr' }, { id: 11, tld: '.ie' }, { id: 12, tld: '.be' }, { id: 13, tld: '.de' }, { id: 14, tld: '.in' }, { id: 17, tld: '.hk' }, { id: 18, tld: '.es' }]
//   for (let site of nf) {
//     const data = await GetCustomReportDataSums(site.id, 'day', 'yesterday')
//     console.log(JSON.stringify(data, null, 2))
//     // const data = await GetPageURLsData(`https://hostpapa${site.tld}`, site.id, 'day', 'yesterday')
//     // console.log(data)
//     // const fdfdg = await GetChannelDataSums(site.id, 'week', 'last1', null)
//     // console.log(fdfdg)
//     // dbfg.push(...fdfdg)
//     // // first, convert data into a Map with reduce
//     // let counts = fdfdg.reduce((prev, curr) => {
//     //   let count = prev.get(curr.label) || 0;
//     //   prev.set(curr.label, curr.visits + count);
//     //   return prev;
//     // }, new Map());

//     // // then, map your counts object back to an array
//     // let reducedObjArr = [...counts].map(([label, visits]) => {
//     //   return { label, visits }
//     // })

//     // console.log(reducedObjArr.map(x => x.visits));
//   }
// //   const labelSums = (label) => dbfg.filter(x => x.label === label).map(x => x.visits).reduce((x, y) => x + y)
// //   const convs = label => dbfg.filter(x => x.label === label).map(x => !x.conversions ? 0 : x.conversions).reduce((x, y) => x + y, 0)

// //   const directEntry = labelSums('Direct Entry')
// //   const campaign = labelSums('Campaigns')
// //   const searchEngines = labelSums('Search Engines')
// //   const websites = labelSums('Websites')
// //   const socialNetwork = labelSums('Social Networks')

// //   const directEntryConv = convs('Direct Entry')
// //   const campaignConv = convs('Campaigns')
// //   const searchEnginesConv = convs('Search Engines')
// //   const websitesConv = convs('Websites')
// //   const socialNetworkConv = convs('Social Networks')

// //   const totalAll = directEntry + campaign + searchEngines + websites + socialNetwork
// //   const totalConv = directEntryConv + campaignConv + searchEnginesConv + websitesConv + socialNetworkConv

// //   console.log([directEntry, campaign, searchEngines, websites, socialNetwork])
// //   console.log([directEntryConv, campaignConv, searchEnginesConv, websitesConv, socialNetworkConv])

// //   console.log(totalAll)
// //   console.log(totalConv)
// })()

async function GetChannelDataSums(idsite, period, date, event) {
  const { data } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Goals.getGoals&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`);
  let vObj = []
  let singArr = []
  let singArr2 = []
  for (let singleData of data) {
    let { name } = singleData;
    if (name === "Shared Hosting Order (EVENT) // Partially Required") {
      let { data: vData } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Referrers.getReferrerType&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)

      let ourData = []
      if (Array.isArray(vData)) {
        ourData.push(...vData)
      } else {
        ourData = Object.values(vData)[0]
      }
      for (let single of ourData) {
        let sharedHostingConversion = single.goals && single.goals['idgoal=4'] && single.goals['idgoal=4'].nb_conversions
        let { label, nb_visits, nb_visits_converted } = single

        // console.log({
        //   label: label,
        //   nb_visits: nb_visits,
        //   nb_visits_converted: nb_visits_converted,
        //   sid: idsite
        // })
        // console.log(`============`)
        singArr.push({
          label: label,
          visits: nb_visits,
          conversions: sharedHostingConversion
        })
      }
    } else if (name === 'WordPress Hosting Order (EVENT) // OOF') {
      let { data: vData } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Referrers.getReferrerType&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)

      let ourData = []

      if (Array.isArray(vData)) {
        ourData.push(vData)
      } else {
        ourData = Object.values(vData)[0]
      }

      for (let single of ourData) {
        let { label, nb_visits, nb_visits_converted } = single
        let wordpressHostingConversion = single.goals && single.goals['idgoal=5'] && single.goals['idgoal=5'].nb_conversions
        singArr2.push({
          label: label,
          visits: nb_visits,
          conversions: wordpressHostingConversion
        })
      }
    }
  }

  if (event && event === 'wp') {
    return singArr2
  } else {
    return singArr
  }
}


async function GetChannelData(idsite, period, date, chart) {

  let chartArray = []
  let chartArrayLabel = []
  let spaceArray = []
  for (let i = 0; i < spaceArray.length; i++) {
    spaceArray.push(" ")
  }

  const req = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Goals.getGoals&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`);
  const ids = req.data.map(x => {
    if (x.name === "Shared Hosting Order (EVENT) // Partially Required" || x.name === 'WordPress Hosting Order (EVENT) // OOF') {
      return x.idgoal
    }
    return null
  })

  const filt = ids.filter(x => x)
  const { data } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Referrers.getReferrerType&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)

  let ourData = []
  if (Array.isArray(data)) {
    ourData.push(...data)
  } else {
    ourData = Object.values(data)[0]
  }


  const dr = []
  for (let i = 0; i < filt.length; i++) {
    const arr = []
    const tArr = []
    const tArrlabel = []
    let visitsingtotal = 0
    let shareTotal = 0
    for (let d of ourData) {
      const { label, nb_visits, goals } = d
      const sharedhostingObj = goals && goals[`idgoal=${filt[i] && filt[i]}`]
      const sharedhostingConv = !sharedhostingObj ? 0 : sharedhostingObj.nb_conversions
      const sharedHostingRev = !sharedhostingObj ? 'CA$0' : `CA$${sharedhostingObj.revenue}`
      const sharedhostingConvRate = !sharedhostingConv ? '0%' : ((sharedhostingConv / nb_visits) * 100).toPrecision(2) + '%'
      visitsingtotal = parseInt(nb_visits, 10) + visitsingtotal
      shareTotal = parseInt(sharedhostingConv, 10) + shareTotal

      const obj = {
        channel_type: label,
        visits: nb_visits,
        sharedHostingConv: sharedhostingConv,
        sharedhostingConvRate: sharedhostingConvRate,
        sharedHostingRev: sharedHostingRev
      }
      arr.push(Object.values(obj))
      // arr.push(obj)
      tArr.push(sharedhostingConv)
      tArrlabel.push(label)
    }
    if (i === 1) {
      dr.push(["CHANNEL", "VISITS", "WORDPRESS HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSIONS", "WORDPRESS HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSION RATE", "WORDPRESS HOSTING ORDER (EVENT) // PARTIALLY REQUIRED REVENUE"], ...arr)
      const sums = tArr.map(x => isNaN((x / shareTotal) * 100) ? 0 : (x / shareTotal) * 100)
      chartArray.push(sums)
    } else {
      dr.push(["CHANNEL", "VISITS", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSIONS", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSION RATE", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED REVENUE"], ...arr)
    }
    dr.push([" ", " ", " ", " ", " "])
    dr.push(["Total", visitsingtotal, shareTotal, " ", " "], " ")
    chartArray.push(tArr)
    chartArrayLabel.push(tArrlabel)
  }

  if (chart) {
    return chartArray[1]
  }
  //   console.log(`RESULT UIS:::`, chartArray[1])
  return dr
}

async function GetCustomReportDataSums(idsite, period, date, label) {
  let sharedHosting = []
  let wordpressHosting = []
  const { data: reportData } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=CustomReports.getConfiguredReports&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)
  for (let singleReport of reportData) {
    const { name, idcustomreport } = singleReport
    if (name === 'Master Report (Medium/Channel - Source/Campaign)') {
      const { data: customReportData } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=CustomReports.getCustomReport&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json&idCustomReport=${idcustomreport}`)

      let ourData = []
      if (Array.isArray(customReportData)) {
        ourData.push(...customReportData)
      } else {
        ourData = Object.values(customReportData)[0]
      }

      for (let cReport of ourData) {
        const { label, nb_visits, goal_4_conversion, goal_5_conversion } = cReport
        sharedHosting.push({
          label: label,
          visits: nb_visits,
          conversion: goal_4_conversion
        })

        wordpressHosting.push({
          label: label,
          visits: nb_visits,
          conversion: goal_5_conversion
        })
      }
    }
  }

  if (label && label === 'wp') {
    return wordpressHosting
  } else {
    return sharedHosting
  }
}


async function GetCustomReportData(idsite, period, date) {

  let spaceArray = []
  for (let i = 0; i < 10; i++) {
    spaceArray.push(" ")
  }
  let visits = []
  let visitsTotal = 0
  let conversionGoalsHostingTotal = 0
  let conversionGoalsWPTotal = 0
  let convertedTotal = 0

  const req = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=CustomReports.getConfiguredReports&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)
  for (let d of req.data) {
    if (d.name === 'Master Report (Medium/Channel - Source/Campaign)') {
      const repID = d.idcustomreport
      const { data: dw } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=CustomReports.getCustomReport&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json&idCustomReport=${repID}`)

      let ourData = []
      if (Array.isArray(dw)) {
        ourData.push(...dw)
      } else {
        ourData = Object.values(dw)[0]
      }


      for (let result of ourData) {
        if (result.label === 'paid-search' || result.label === 'affiliate' || result.label === 'email' || result.label === 'feedersites') {
          let avg_gentime = !result.avg_page_generation_time ? '0s' : (result.avg_page_generation_time / 1000).toPrecision(3) + 's'
          // we want to look for the shared hosting goal id and the wordpress hosting goal id
          let { data } = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Goals.getGoals&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`);
          const ids = []
          for (let x of data) {
            if (x.name === "Shared Hosting Order (EVENT) // Partially Required") {
              // console.log(`SHARED HOSTING ID:: ${x.idgoal}`)
              ids.push(x.idgoal)
            } else if (x.name === 'WordPress Hosting Order (EVENT) // OOF') {
              // console.log(`WORDPRESS ID: ${x.idgoal}`)
              // console.log(`=======`)
              ids.push(x.idgoal)
            }
          }

          const [sharedHosting, wordpress] = ids
          const v1 = sharedHosting && result[`goal_${sharedHosting}_conversion`]
          const v2 = wordpress && result[`goal_${wordpress}_conversion`]

          const v1_rate = sharedHosting && result[`goal_${sharedHosting}_conversion_uniq_visitors_rate`]
          const v2_rate = sharedHosting && result[`goal_${wordpress}_conversion_uniq_visitors_rate`]

          visitsTotal = result.nb_visits + visitsTotal
          conversionGoalsHostingTotal = v1 + conversionGoalsHostingTotal
          conversionGoalsWPTotal = v2 + conversionGoalsWPTotal
          convertedTotal = result.nb_visits_converted + convertedTotal

          let avg_time_per_v = !result.avg_sum_corehome_visittotaltime_per_visits ? 0 : parseInt((result.avg_sum_corehome_visittotaltime_per_visits / 60).toPrecision(3), 10)
          visits.push([result.label, result.nb_visits, result.bounce_rate, avg_gentime, avg_time_per_v, v1, ((v1_rate * 100).toPrecision(3) + '%'), !v2 ? " " : v2, v2_rate * 100, result.nb_visits_converted])
        }
      }
    }
  }
  visits.push([...spaceArray])
  visits.push(["Total", visitsTotal, " ", " ", " ", conversionGoalsHostingTotal, " ", conversionGoalsWPTotal, " ", convertedTotal])
  return visits
}
async function GetFunnelData(url, idsite, period, date) {
  const req = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Goals.getGoals&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`);
  const ids = req.data.map(x => {
    if (x.name === "Shared Hosting Order (EVENT) // Partially Required" || x.name === 'WordPress Hosting Order (EVENT) // OOF') {
      return {
        n: idsite,
        Id: x.idgoal,
        name: x.name
      }
    }
  })

  const goalIDS = ids.filter((x, y) => {
    if (x && x.n === 12 || x && x.n === 13 || x && x.n === 14 || x && x.n === 17 || x && x.n === 18) {
      return {
        n: x.n,
        Id: null,
        name: null
      }
    } else return x
  })
  // console.log(goalIDS)
  const d = []
  for (let res of goalIDS) {
    const { Id, name } = res
    const r = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Funnels.getMetrics&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json&idGoal=${Id}`)
    const { data } = r

    let ourData = {}
    if (Object.values(data).length === 1) {
      ourData = Object.values(data)[0]
    } else {
      ourData = data
    }
    const { funnel_sum_entries, funnel_sum_exits, funnel_nb_conversions, funnel_conversion_rate, funnel_abandoned_rate } = ourData
    d.push({
      GOAL_ID: Id,
      GOAL_NAME: name,
      FUNNEL_SUM_ENTRIES: funnel_sum_entries || '',
      FUNNEL_SUM_EXITS: funnel_sum_exits || '',
      FUNNEL_NB_CONVERSIONS: funnel_nb_conversions || '',
      FUNNEL_CONVERSION_RATE: funnel_conversion_rate || '',
      FUNNEL_ABANDONED_RATE: funnel_abandoned_rate || ''
    })
  }

  return {
    id: idsite,
    data: d
  }
}


async function GetTrafficSources(url, idsite, period, date) {
  const req = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Referrers.getReferrerType&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`);
  let ourData = []
      if (Array.isArray(req.data)) {
        ourData.push(...req.data)
      } else {
        ourData = Object.values(req.data)[0]
      }
  const result = ourData.map(x => {
    const { label, nb_visits, bounce_count, goals } = x
    let bounce_perc = bounce_count / nb_visits
    const dg = {
      label: label,
      visits: nb_visits,
      bounce: `${bounce_perc.toPrecision(3)}%`
    }
    return Object.values(dg)
  })
  return result
}


async function GetPageURLsData(url, idsite, period, date) {
  const req = await axios(`https://hstng.info/index.php?token_auth=db02ef15f703a1e211555f9a457d08e1&module=API&method=Actions.getPageUrls&idSite=${idsite}&rec=1&date=${date}&period=${period}&format=json`)
  let ourData = []
      if (Array.isArray(req.data)) {
        ourData.push(...req.data)
      } else {
        ourData = Object.values(req.data)[0]
      }
  const result = ourData.map(x => {
    const { nb_visits: pageViews, avg_time_on_page: timeOnPage, bounce_rate: bounceRate, exit_rate: exitRate, avg_time_generation: avgTimeGen, nb_uniq_visitors: uniqViews, label } = x
    const pageURL = label.replace('/', '')

    const dta = {
      pageURL: `${url}/${pageURL}`,
      pageViews: pageViews,
      uniquePageViews: uniqViews ? uniqViews : 0,
      bounceRate: bounceRate,
      avgTimeOnPage: timeOnPage,
      exitRate: exitRate,
      avgGenTime: avgTimeGen
    }
    const vals = Object.values(dta)
    return vals
  })
  return result
}

module.exports = { GetPageURLsData, GetTrafficSources, GetFunnelData, GetCustomReportData, GetChannelData, GetChannelDataSums, GetCustomReportDataSums }