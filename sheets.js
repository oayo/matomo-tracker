/**
 * THIS FILE IS REALLY A HUGE MESS. FOR EXAMPLE, BATCH THE SHEETS CREATION REQUESTS OR EVEN THROTTLE REQUEST TO AVOID ERROR LIMIT
 * TODO: change variable names to something understandable. 
 */
const { google } = require('googleapis')
const matomo = require('piwik').setup('https://hstng.info/piwik.php?token_auth=db02ef15f703a1e211555f9a457d08e1')
const moment = require('moment');
const chalk = require('chalk');
const { GetPageURLsData, GetTrafficSources, GetFunnelData, GetCustomReportData, GetChannelData } = require('./matomo');
const { MediumDataSums, ChannelDataSums } = require('./rollup')
const pastDay = moment().subtract(1, 'day')

const formattedDayDate = pastDay.format("YYYY-MM-DD")

async function GetSheet(auth, date, period) {
  try {
    const sheet = await google.sheets({
      version: 'v4',
      auth: auth
    });

    // we want to append
    const allSiteAndID = [{ id: 2, tld: '.ca' }, { id: 3, tld: '.com' }, { id: 4, tld: '.com.au' }, { id: 5, tld: '.eu' }, { id: 6, tld: 'co.nz' }, { id: 7, tld: '.sg' }, { id: 8, tld: 'co.uk' }, { id: 9, tld: '.com.mx' }, { id: 10, tld: '.fr' }, { id: 11, tld: '.ie' }, { id: 12, tld: '.be' }, { id: 13, tld: '.de' }, { id: 14, tld: '.in' }, { id: 17, tld: '.hk' }, { id: 18, tld: '.es' }]

    // create a new googlesheet
    const newSpreadSheet = await sheet.spreadsheets.create({
      resource: {
        properties: {
          title: period === 'week' ? `Matomo weekly data for week ${moment().format('w') - 1}` : (period === 'month' ? `Matomo Monthly data for ${moment().format('MMMM')}` : `Matomo daily data ${date ? date : formattedDayDate}`)
        }
      }
    })

    // console.log(`NEWLY CREATED SHEET IS:: ${JSON.stringify(newSheet, null, 2)}`)
    const { data: { spreadsheetId, sheets, spreadsheetUrl, properties: { title } } } = newSpreadSheet

    // NEW SHEET FOR ROLLUP
    const newRollUpSheet = await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        requests: [
          {
            addSheet: {
              properties: {
                title: `ROLLUP`,
                index: 0
              },
            }
          }
        ]
      }
    })
    const { data: { replies } } = newRollUpSheet
    let { sheetId } = replies[0].addSheet.properties

    let lvv = []
    let chanVisitsSum = await ChannelDataSums('shared', 'visits', date, period)
    let chanConvSum = await ChannelDataSums('shared', 'conv', date, period)
    for (let i = 0; i < chanVisitsSum.length; i++) {
      let title = ['Direct Entry', 'Campaigns', 'Search Engines', 'Websites', 'Social Networks']

      lvv.push({
        values: [{
          userEnteredValue: {
            stringValue: title[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: chanVisitsSum[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: chanConvSum[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }]
      })
    }
    // WORDPRESS HOSTING EQUIVALENT OF ABOVE
    let lvv2 = []
    let chanVisitsSum2 = await ChannelDataSums('wp', 'visits', date, period)
    let chanConvSum2 = await ChannelDataSums('wp', 'conv', date, period)
    for (let i = 0; i < chanVisitsSum2.length; i++) {
      let title = ['Direct Entry', 'Campaigns', 'Search Engines', 'Websites', 'Social Networks']

      lvv2.push({
        values: [{
          userEnteredValue: {
            stringValue: title[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: chanVisitsSum[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: chanConvSum2[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 29325,
              "green": 54315,
              "blue": 42840,
              "alpha": 0
            }
          }
        }]
      })
    }


    // MEDIUM PART
    let lvm = []
    let mdVisitSum = await MediumDataSums('shared', 'visits', date, period)
    let mdConv1 = await MediumDataSums('shared', 'conv', date, period)
    let mdConv2 = await MediumDataSums('wp', 'conv', date, period)

    for (let i = 0; i < mdVisitSum.length; i++) {
      let title = ['affiliate', 'email', 'paid-search', 'feedersites']

      lvm.push({
        values: [{
          userEnteredValue: {
            stringValue: title[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 24225,
              "green": 53040,
              "blue": 63240,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: mdVisitSum[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 24225,
              "green": 53040,
              "blue": 63240,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: mdConv1[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 24225,
              "green": 53040,
              "blue": 63240,
              "alpha": 0
            }
          }
        }, {
          userEnteredValue: {
            numberValue: mdConv2[i]
          }, userEnteredFormat: {
            backgroundColor: {
              "red": 24225,
              "green": 53040,
              "blue": 63240,
              "alpha": 0
            }
          }
        }]
      })
    }


    await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        requests: [
          {
            appendCells: {
              sheetId: sheetId,
              rows: [{
                values: [{
                  userEnteredValue: {
                    stringValue: "CHANNEL",
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: "VISITS"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: "SHARED HOSTING ORDER (EVENT) //PARTIALLY REQUIRED CONVERSIONS"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }],
              }, ...lvv, {
                values: [{
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }]
              }, {
                values: [{
                  userEnteredValue: {
                    stringValue: "Total"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: chanVisitsSum.reduce((x, y) => x + y)
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: chanConvSum.reduce((x, y) => x + y)
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }]
              }, {
                values: [{
                  userEnteredValue: {
                    stringValue: " "
                  }
                }]
              },
              // WORDPRESS HOSTING PART
              {
                values: [{
                  userEnteredValue: {
                    stringValue: "CHANNEL",
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: "VISITS"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: "WORDPRESS HOSTING ORDER (EVENT) //PARTIALLY REQUIRED CONVERSIONS"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }],
              }, ...lvv2, {
                values: [{
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }]
              }, {
                values: [{
                  userEnteredValue: {
                    stringValue: "Total"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: chanVisitsSum2.reduce((x, y) => x + y)
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: chanConvSum2.reduce((x, y) => x + y)
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 29325,
                      "green": 54315,
                      "blue": 42840,
                      "alpha": 0
                    }
                  }
                }]
              }, {
                values: [{
                  userEnteredValue: {
                    stringValue: " "
                  }
                }]
              },

              // MEDIUM PART
              {
                values: [{
                  userEnteredValue: {
                    stringValue: "MEDIUM",
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: "VISITS"
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: 'CONVERSIONS GOAL "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED'
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: 'CONVERSIONS GOAL "WORDPRESS HOSTING ORDER (EVENT) // OOF'
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }],
              }, ...lvm, {
                values: [{
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    stringValue: " "
                  }, userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }]
              }, {
                values: [{
                  userEnteredValue: {
                    stringValue: "Total"
                  },
                  userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: mdVisitSum.reduce((x, y) => x + y)
                  },
                  userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: mdConv1.reduce((x, y) => x + y)
                  },
                  userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }, {
                  userEnteredValue: {
                    numberValue: mdConv2.reduce((x, y) => x + y)
                  },
                  userEnteredFormat: {
                    backgroundColor: {
                      "red": 24225,
                      "green": 53040,
                      "blue": 63240,
                      "alpha": 0
                    }
                  }
                }]
              }

              ], fields: '*'
            }
          }
        ]
      }
    })

    // CHARTS FOR ROLLUP
    await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        "requests": [
          {
            "addChart": {
              "chart": {
                "spec": {
                  "title": "SHARED HOSTING ORDERS",
                  "pieChart": {
                    // "legendPosition": "RIGHT_LEGEND",
                    "threeDimensional": false,
                    "domain": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 1,
                            "endRowIndex": 6,
                            "startColumnIndex": 0,
                            "endColumnIndex": 1
                          }
                        ]
                      }
                    },
                    "series": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 1,
                            "endRowIndex": 6,
                            "startColumnIndex": 2,
                            "endColumnIndex": 3
                          }
                        ]
                      }
                    },
                  }
                },
                "position": {
                  "overlayPosition": {
                    "anchorCell": {
                      "sheetId": sheetId,
                      "rowIndex": 1,
                      "columnIndex": 7
                    },
                    "widthPixels": 300,
                    "heightPixels": 185
                  }
                }
              }
            }
          }
        ]
      }
    })


    await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        "requests": [
          {
            "addChart": {
              "chart": {
                "spec": {
                  "title": "WORDPRESS HOSTING ORDERS",
                  "pieChart": {
                    // "legendPosition": "RIGHT_LEGEND",
                    "threeDimensional": false,
                    "domain": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 10,
                            "endRowIndex": 15,
                            "startColumnIndex": 0,
                            "endColumnIndex": 1
                          }
                        ]
                      }
                    },
                    "series": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 10,
                            "endRowIndex": 15,
                            "startColumnIndex": 2,
                            "endColumnIndex": 3
                          }
                        ]
                      }
                    },
                  }
                },
                "position": {
                  "overlayPosition": {
                    "anchorCell": {
                      "sheetId": sheetId,
                      "rowIndex": 1,
                      "columnIndex": 11
                    },
                    "widthPixels": 300,
                    "heightPixels": 185
                  }
                }
              }
            }
          }
        ]
      }
    })

    // CHART FOR ROLLUP --PAID
    await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        "requests": [
          {
            "addChart": {
              "chart": {
                "spec": {
                  "title": "SHARED HOSTING ORDERS (PAID)",
                  "pieChart": {
                    // "legendPosition": "RIGHT_LEGEND",
                    "threeDimensional": false,
                    "domain": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 19,
                            "endRowIndex": 23,
                            "startColumnIndex": 0,
                            "endColumnIndex": 1
                          }
                        ]
                      }
                    },
                    "series": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 19,
                            "endRowIndex": 23,
                            "startColumnIndex": 1,
                            "endColumnIndex": 2
                          }
                        ]
                      }
                    },
                  }
                },
                "position": {
                  "overlayPosition": {
                    "anchorCell": {
                      "sheetId": sheetId,
                      "rowIndex": 11,
                      "columnIndex": 7
                    },
                    "widthPixels": 300,
                    "heightPixels": 185
                  }
                }
              }
            }
          }
        ]
      }
    })

    await sheet.spreadsheets.batchUpdate({
      spreadsheetId: spreadsheetId,
      resource: {
        "requests": [
          {
            "addChart": {
              "chart": {
                "spec": {
                  "title": "WORDPRESS ORDERS (PAID)",
                  "pieChart": {
                    // "legendPosition": "RIGHT_LEGEND",
                    "threeDimensional": false,
                    "domain": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 19,
                            "endRowIndex": 23,
                            "startColumnIndex": 0,
                            "endColumnIndex": 1
                          }
                        ]
                      }
                    },
                    "series": {
                      "sourceRange": {
                        "sources": [
                          {
                            "sheetId": sheetId,
                            "startRowIndex": 19,
                            "endRowIndex": 23,
                            "startColumnIndex": 2,
                            "endColumnIndex": 3
                          }
                        ]
                      }
                    },
                  }
                },
                "position": {
                  "overlayPosition": {
                    "anchorCell": {
                      "sheetId": sheetId,
                      "rowIndex": 11,
                      "columnIndex": 11
                    },
                    "widthPixels": 300,
                    "heightPixels": 185
                  }
                }
              }
            }
          }
        ]
      }
    })




    let vTotalDirectEntrySharedHosting = 0
    // for each ```allSiteID```, create a new googlesheet with the siteID.
    // for each of the ```newSheet``` response, append a new row that contains the page traffic data.
    // then for other sources (source and conversion, create like this table but different titles)
    // create a new sheet within the spreadsheet
    for (let [index, site] of allSiteAndID.entries()) {
      let { id, tld } = site

      const mainData = await GetPageURLsData(`https://hostpapa${tld}`, id, period ? period : period ? period : 'day', date ? date : 'yesterday')
      const mainDataArr = []

      for (let d of mainData) {
        let cnt = []
        // const [one, two, three, four, five, six, seven] = d
        for (let ddd of d) {
          cnt.push({
            userEnteredValue: {
              stringValue: ddd.toString()
            }
          })
        }

        mainDataArr.push({ values: [...cnt] })
      }

      let remainingLength = 70 - mainData.length
      const chnMain = []
      const chData = await GetChannelData(id, period ? period : 'day', date ? date : 'yesterday')
      // const chD = await GetChannelData(id, period ? period : 'day', date ? date : 'yesterday', true)
      for (let [index, chnData] of chData.entries()) {
        // skipping the arrays that dont contain the data we need.
        // if (index !== 0) {
        // console.log(`DIRECT ENTRY SHITT IS : ${JSON.stringify(chnData[1], null, 2)}`)
        let vDirectEntrySharedHosting = !parseInt(chnData[1], 10) ? 0 : parseInt(chnData[1], 10)
        vTotalDirectEntrySharedHosting = vDirectEntrySharedHosting + vTotalDirectEntrySharedHosting
        // }
        // console.log(`EACH ENTRIES IS::`, JSON.stringify(chnData, null, 2))
        const chDone = []
        const chSpace = []

        const chS = []
        if (chnData === " ") {
          for (let i = 0; i < 9; i++) {
            chDone.push({
              userEnteredValue: {
                stringValue: " "
              }
            })
          }
        } else {
          for (let i = 0; i < chnData.length; i++) {
            // console.log(`CH IS .... `, JSON.stringify(parseInt(chnData[i],), null, 2))
            // console.log(`TYPEOF OF CH IS`, typeof chData[i])
            let ps = !parseInt(chnData[i], 10) ? String(chnData[i]) : parseInt(chnData[i], 10)

            if (chnData[i] === 0) {
              chDone.push({
                userEnteredValue: {
                  numberValue: parseInt(0, 10)
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 29325,
                    "green": 54315,
                    "blue": 42840,
                    "alpha": 0
                  }
                }
              })
            } else if (chnData[i] === " ") {
              chDone.push({
                userEnteredValue: {
                  numberValue: parseInt(chnData[i], 10)
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 29325,
                    "green": 54315,
                    "blue": 42840,
                    "alpha": 0
                  }
                }
              })
            }

            if (typeof ps === 'string') {
              chDone.push({
                userEnteredValue: {
                  stringValue: String(chnData[i])
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 29325,
                    "green": 54315,
                    "blue": 42840,
                    "alpha": 0
                  }
                }
              })
            } else if (typeof ps === 'number') {
              chDone.push({
                userEnteredValue: {
                  numberValue: ps
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 29325,
                    "green": 54315,
                    "blue": 42840,
                    "alpha": 0
                  }
                }
              })
            } else {
              chDone.push({
                userEnteredValue: {
                  numberValue: ps
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 29325,
                    "green": 54315,
                    "blue": 42840,
                    "alpha": 0
                  }
                }
              })
            }
          }
        }

        let bs = {
          values: [...chDone, chSpace]
        }

        chnMain.push(bs)
      }
      // console.log(`TOTSSS: ${chVisitsTotal / 2} AND ${chHostingOrder}`)
      const trafficSourceData = await GetTrafficSources(`https://hostpapa${tld}`, id, period ? period : 'day', date ? date : 'yesterday')
      const customReport = await GetCustomReportData(id, period ? period : 'day', date ? date : 'yesterday')
      // const customReport = [dOne, dTwo, dThree, dFour]
      const main = []
      for (let eachOne of customReport) {
        const dt = []
        if (!eachOne) {
          continue
        }

        for (let i = 0; i < eachOne.length; i++) {
          if (typeof eachOne[i] === 'string') {

            if (eachOne[i] === " ") {
              dt.push({
                userEnteredValue: {
                  numberValue: 0
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 24225,
                    "green": 53040,
                    "blue": 63240,
                    "alpha": 0
                  }
                }
              })
            } else {
              dt.push({
                userEnteredValue: {
                  stringValue: String(eachOne[i])
                },
                userEnteredFormat: {
                  backgroundColor: {
                    "red": 24225,
                    "green": 53040,
                    "blue": 63240,
                    "alpha": 0
                  }
                }
              })
            }
          } else if (typeof eachOne[i] === 'number') {
            dt.push({
              userEnteredValue: {
                numberValue: eachOne[i]
              },
              userEnteredFormat: {
                backgroundColor: {
                  "red": 24225,
                  "green": 53040,
                  "blue": 63240,
                  "alpha": 0
                }
              }
            })
          } else {
            dt.push({
              userEnteredValue: {
                stringValue: String(eachOne[i])
              },
              userEnteredFormat: {
                backgroundColor: {
                  "red": 24225,
                  "green": 53040,
                  "blue": 63240,
                  "alpha": 0
                }
              }
            })
          }
        }

        const bs = {
          values: [...dt]
        }

        main.push(bs)
      }
      const trafficSourceDataArr = []

      for (let single of trafficSourceData) {
        let tmp = []

        for (let singletemp of single) {
          tmp.push({
            userEnteredValue: {
              stringValue: singletemp.toString()
            }
          })
        }

        trafficSourceDataArr.push({ values: [...tmp] })
      }
      // console.log(`MAIN ARR IS::${JSON.stringify(mainDataArr, null, 2)}`)

      const funnels = await GetFunnelData(null, id, period ? period : 'day', date ? date : 'yesterday')
      const { id: fid, data } = funnels
      const [one, two] = data
      const newSheet = await sheet.spreadsheets.batchUpdate({
        spreadsheetId: spreadsheetId,
        resource: {
          requests: [
            {
              addSheet: {
                properties: {
                  title: `${tld}`,
                  index: index + 1
                },
              }
            }
          ]
        }
      })

      const { data: { replies } } = newSheet
      for (let reply of replies) {
        const { addSheet: { properties: { sheetId } } } = reply
        await sheet.spreadsheets.batchUpdate({
          spreadsheetId: spreadsheetId,
          resource: {
            requests: [
              {
                appendCells: {
                  sheetId: sheetId,
                  rows: [{
                    values: [{
                      userEnteredValue: {
                        stringValue: "PAGE URL",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "PAGE VIEWS",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "UNIQUE PAGE VIEWS",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "BOUNCE RATE",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "AVG. TIME ON PAGE",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "EXIT RATE",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "AVG. GENERATION TIME",
                      }
                    }]
                  }, ...mainDataArr, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "  ",
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "CHANNEL TYPE",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "NUMBER OF VISITS",
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: "BOUNCE RATE",
                      }
                    }]
                  }, ...trafficSourceDataArr, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "  ",
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "  ",
                      }
                    }]
                  }],
                  fields: '*'
                }
              }
            ]
          }
        })


        if (id === 12 || id === 13 || id === 14 || id === 17 || id === 18) {
          continue;
        }


        await sheet.spreadsheets.batchUpdate({
          spreadsheetId: spreadsheetId,
          resource: {
            requests: [
              {
                appendCells: {
                  sheetId: sheetId,
                  rows: [{
                    values: [{
                      userEnteredValue: {
                        stringValue: "GOAL ID"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.GOAL_ID.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "GOAL NAME"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.GOAL_NAME.toString()
                      }
                    }]
                  },
                  {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL SUM ENTRIES"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.FUNNEL_SUM_ENTRIES.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL SUM EXITS"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.FUNNEL_SUM_EXITS.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL NUM. OF CONVERSION"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.FUNNEL_NB_CONVERSIONS.toString()
                      }
                    }],
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL CONVERSION RATE"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.FUNNEL_CONVERSION_RATE.toString()
                      }
                    }],
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL ABANDONED RATE"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: one.FUNNEL_ABANDONED_RATE.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "  ",
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "GOAL ID"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.GOAL_ID.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "GOAL NAME"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.GOAL_NAME.toString()
                      }
                    }]
                  },
                  {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL SUM ENTRIES"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.FUNNEL_SUM_ENTRIES.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL SUM EXITS"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.FUNNEL_SUM_EXITS.toString()
                      }
                    }]
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL NUM. OF CONVERSION"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.FUNNEL_NB_CONVERSIONS.toString()
                      }
                    }],
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL CONVERSION RATE"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.FUNNEL_CONVERSION_RATE.toString()
                      }
                    }],
                  }, {
                    values: [{
                      userEnteredValue: {
                        stringValue: "FUNNEL ABANDONED RATE"
                      }
                    }, {
                      userEnteredValue: {
                        stringValue: two.FUNNEL_ABANDONED_RATE.toString()
                      }
                    }]
                  }],
                  fields: '*'
                }
              }
            ]
          }
        })
        /**
         * 
         * @param {string} rgb_color 
         * convert our rgb from google to the format accepted by gsheet
         */
        var protoToCssColor = function (rgb_color) {
          var redFrac = rgb_color.red || 0.0;
          var greenFrac = rgb_color.green || 0.0;
          var blueFrac = rgb_color.blue || 0.0;
          var red = Math.floor(redFrac * 255);
          var green = Math.floor(greenFrac * 255);
          var blue = Math.floor(blueFrac * 255);

          if (!('alpha' in rgb_color)) {
            return rgbToCssColor_(red, green, blue);
          }

          var alphaFrac = rgb_color.alpha.value || 0.0;
          var rgbParams = [red, green, blue].join(',');
          return ['rgba(', rgbParams, ',', alphaFrac, ')'].join('');
        };

        var rgbToCssColor_ = function (red, green, blue) {
          var rgbNumber = new Number((red << 16) | (green << 8) | blue);
          var hexString = rgbNumber.toString(16);
          var missingZeros = 6 - hexString.length;
          var resultBuilder = ['#'];
          for (var i = 0; i < missingZeros; i++) {
            resultBuilder.push('0');
          }
          resultBuilder.push(hexString);
          return resultBuilder.join('');
        };

        let colors = protoToCssColor({
          red: 248,
          green: 188,
          blue: 95,
          alpha: 0
        })

        if (id === 12 || id === 13 || id === 17 || id === 18) {
          continue;
        }


        let sharedhostingTitleCells = []
        let sharedhostingTitleTexts = ["CHANNEL", "VISITS", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSIONS", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSION RATE", "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED REVENUE"]

        for (let i = 0; i < sharedhostingTitleTexts.length; i++) {
          sharedhostingTitleCells.push({
            userEnteredValue: {
              stringValue: sharedhostingTitleTexts[i]
            },
            userEnteredFormat: {
              backgroundColor: {
                "red": 24225,
                "green": 53040,
                "blue": 63240,
                "alpha": 0
              }
            }
          })
        }

        let sharehostingStartingIndex = mainDataArr.length + trafficSourceDataArr.length + 1 + funnels.data.length + 18
        let sharehostingEndingIndex = sharehostingStartingIndex + lvv.length

        let wordpresshostingStartingIndex = sharehostingEndingIndex + (trafficSourceData.length === 4 ? 3 : 4)
        let wordpresshostingEndingIndex = wordpresshostingStartingIndex + lvv.length

        await sheet.spreadsheets.batchUpdate({
          spreadsheetId: spreadsheetId,
          resource: {
            requests: [
              {
                appendCells: {
                  sheetId: sheetId,
                  rows: [...chnMain],
                  fields: '*'
                }
              }
            ]
          }
        })

        await sheet.spreadsheets.batchUpdate({
          spreadsheetId: spreadsheetId,
          resource: {
            "requests": [
              {
                "addChart": {
                  "chart": {
                    "spec": {
                      "title": "SHARED HOSTING ORDERS",
                      "pieChart": {
                        // "legendPosition": "RIGHT_LEGEND",
                        "threeDimensional": false,
                        "domain": {
                          "sourceRange": {
                            "sources": [
                              {
                                "sheetId": sheetId,
                                "startRowIndex": sharehostingStartingIndex,
                                "endRowIndex": sharehostingEndingIndex,
                                "startColumnIndex": 0,
                                "endColumnIndex": 1
                              }
                            ]
                          }
                        },
                        "series": {
                          "sourceRange": {
                            "sources": [
                              {
                                "sheetId": sheetId,
                                "startRowIndex": sharehostingStartingIndex,
                                "endRowIndex": sharehostingEndingIndex,
                                "startColumnIndex": 2,
                                "endColumnIndex": 3
                              }
                            ]
                          }
                        },
                      }
                    },
                    "position": {
                      "overlayPosition": {
                        "anchorCell": {
                          "sheetId": sheetId,
                          "rowIndex": mainDataArr.length + 13,
                          "columnIndex": 7
                        },
                        "widthPixels": 300,
                        "heightPixels": 185
                      }
                    }
                  }
                }
              }
            ]
          }
        })


        await sheet.spreadsheets.batchUpdate({
          spreadsheetId: spreadsheetId,
          resource: {
            "requests": [
              {
                "addChart": {
                  "chart": {
                    "spec": {
                      "title": "WORDPRESS HOSTING ORDERS",
                      "pieChart": {
                        // "legendPosition": "RIGHT_LEGEND",
                        "threeDimensional": false,
                        "domain": {
                          "sourceRange": {
                            "sources": [
                              {
                                "sheetId": sheetId,
                                "startRowIndex": wordpresshostingStartingIndex,
                                "endRowIndex": wordpresshostingEndingIndex,
                                "startColumnIndex": 0,
                                "endColumnIndex": 1
                              }
                            ]
                          }
                        },
                        "series": {
                          "sourceRange": {
                            "sources": [
                              {
                                "sheetId": sheetId,
                                "startRowIndex": wordpresshostingStartingIndex,
                                "endRowIndex": wordpresshostingEndingIndex,
                                "startColumnIndex": 2,
                                "endColumnIndex": 3
                              }
                            ]
                          }
                        },
                      }
                    },
                    "position": {
                      "overlayPosition": {
                        "anchorCell": {
                          "sheetId": sheetId,
                          "rowIndex": mainDataArr.length + 13,
                          "columnIndex": 11
                        },
                        "widthPixels": 300,
                        "heightPixels": 185
                      }
                    }
                  }
                }
              }
            ]
          }
        })


        if (id === 2 || id === 3 || id === 4) {
          console.log(`COL IS::`, colors)
          const titleCells = []
          const allCellsText = ["MEDIUM", "VISITS", "BOUNCE RATE", "AVG. GENERATION TIME", "AVG. VISIT DURATION PER VISIT", `CONVERSIONS GOAL "SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED"`, "GOAL SHARED HOSTING ORDER (EVENT) // PARTIALLY REQUIRED CONVERSION RATE", `CONVERSIONS GOAL "WORDPRESS HOSTING ORDER (EVENT) // OOF`, "GOAL WORDPRESS HOSTING ORDER (EVENT) // OOF CONVERSION RATE", "VISITS WITH CONVERSIONS"]

          const spacingCells = []
          for (let i = 0; i < 10; i++) {
            spacingCells.push({
              userEnteredValue: {
                stringValue: " "
              },
              userEnteredFormat: {
                backgroundColor: {
                  "red": 24225,
                  "green": 53040,
                  "blue": 63240,
                  "alpha": 0
                }
              }
            })
          }
          for (let i = 0; i < allCellsText.length; i++) {
            titleCells.push({
              userEnteredValue: {
                stringValue: allCellsText[i]
              },
              userEnteredFormat: {
                backgroundColor: {
                  "red": 24225,
                  "green": 53040,
                  "blue": 63240,
                  "alpha": 0
                }
              }
            })
          }
          await sheet.spreadsheets.batchUpdate({
            spreadsheetId: spreadsheetId,
            resource: {
              requests: [
                {
                  appendCells: {
                    sheetId: sheetId,
                    rows: [{ values: [...titleCells] }, ...main],
                    fields: '*'
                  }
                }
              ]
            }
          })

          await sheet.spreadsheets.batchUpdate({
            spreadsheetId: spreadsheetId,
            resource: {
              "requests": [
                {
                  "addChart": {
                    "chart": {
                      "spec": {
                        "title": "SHARED HOSTING ORDERS (PAID)",
                        "pieChart": {
                          // "legendPosition": "RIGHT_LEGEND",
                          "threeDimensional": false,
                          "domain": {
                            "sourceRange": {
                              "sources": [
                                {
                                  "sheetId": sheetId,
                                  "startRowIndex": wordpresshostingEndingIndex + 4,
                                  "endRowIndex": wordpresshostingEndingIndex + 9,
                                  "startColumnIndex": 0,
                                  "endColumnIndex": 1
                                }
                              ]
                            }
                          },
                          "series": {
                            "sourceRange": {
                              "sources": [
                                {
                                  "sheetId": sheetId,
                                  "startRowIndex": wordpresshostingEndingIndex + 4,
                                  "endRowIndex": wordpresshostingEndingIndex + 9,
                                  "startColumnIndex": 5,
                                  "endColumnIndex": 6
                                }
                              ]
                            }
                          },
                        }
                      },
                      "position": {
                        "overlayPosition": {
                          "anchorCell": {
                            "sheetId": sheetId,
                            "rowIndex": mainDataArr.length + 27,
                            "columnIndex": 7
                          },
                          "widthPixels": 300,
                          "heightPixels": 185
                        }
                      }
                    }
                  }
                }
              ]
            }
          })

          await sheet.spreadsheets.batchUpdate({
            spreadsheetId: spreadsheetId,
            resource: {
              "requests": [
                {
                  "addChart": {
                    "chart": {
                      "spec": {
                        "title": "WORDPRESS ORDERS (PAID)",
                        "pieChart": {
                          // "legendPosition": "RIGHT_LEGEND",
                          "threeDimensional": false,
                          "domain": {
                            "sourceRange": {
                              "sources": [
                                {
                                  "sheetId": sheetId,
                                  "startRowIndex": wordpresshostingEndingIndex + 4,
                                  "endRowIndex": wordpresshostingEndingIndex + 9,
                                  "startColumnIndex": 0,
                                  "endColumnIndex": 1
                                }
                              ]
                            }
                          },
                          "series": {
                            "sourceRange": {
                              "sources": [
                                {
                                  "sheetId": sheetId,
                                  "startRowIndex": wordpresshostingEndingIndex + 4,
                                  "endRowIndex": wordpresshostingEndingIndex + 9,
                                  "startColumnIndex": 7,
                                  "endColumnIndex": 8
                                }
                              ]
                            }
                          },
                        }
                      },
                      "position": {
                        "overlayPosition": {
                          "anchorCell": {
                            "sheetId": sheetId,
                            "rowIndex": mainDataArr.length + 27,
                            "columnIndex": 11
                          },
                          "widthPixels": 300,
                          "heightPixels": 185
                        }
                      }
                    }
                  }
                }
              ]
            }
          })


        }

      }
    }

    // get the sheets data
    // const de = await sheet.spreadsheets.get({
    //     spreadsheetId: spreadsheetId
    // })

    // console.log(`SHEETS DATA GOTTEN IS:::`, JSON.stringify(de.data.sheets.map(x => x.data), null, 2))
    // console.log(`SUM OF ALL DIRECT ENTRY FOR ALL SITEIDS IS: `, JSON.stringify(vTotalDirectEntrySharedHosting, null, 2))
    console.log(`CRAWLED... ALL DONE AND DUSTED:::`)
  } catch (e) {
    console.log(`ERROR GETTING THE SHEET`)
    if (e && e.response && e.response.status === 429) {
      console.log(`RATE LIMIT REACHED!!!`)
    } else {
      console.log(e)
    }
  }
}

module.exports = { GetSheet }